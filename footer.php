<?php
/**
 * The template for displaying the footer
 */
?>
    </div><!-- /.container -->

    <footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p><img alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo get_field('logo_footer', 'option'); ?>" class="img-responsive footer-logo"></p>
				</div>
				<div class="col-md-8">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class'     => 'nav nav-pills pull-right',
						) );
					?>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>