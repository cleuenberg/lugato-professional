<?php
/**
 * The standard template for displaying archive pages
 */

get_header(); ?>

	<?php
		//START: Slider
		$args = array(
		    'post_type'  => 'slide',
			'slide-page' => 'produktkategorien',
			'post_status' => 'publish'
		);
		$slides = get_posts( $args );

		if( $slides ): ?>
			<div id="wooslider-id-666" class="wooslider wooslider-id-666 wooslider-type-attachments wooslider-theme-default">
		        <ul class="slides">
		            <?php foreach( $slides as $post ): ?>
		            	<?php
		            		//meta fields
		            		$slide_meta = get_post_meta( $post->ID );
		            	?>
		                <li class="slide slide-<?php echo $post->post_name; ?>">
		                	<div class="slide-image">
		                		<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
		                	</div>
		                	<div class="slide-content">
		                    	<h2><?php echo $post->post_title; ?></h2>
		                    	<?php echo $post->post_content; ?>
		                    	<p><a href="<?php echo $slide_meta['_wooslider_url'][0]; ?>">Mehr Infos</a></p>
		                    </div>
		                </li>
		            <?php endforeach; ?>
		        </ul>
		    </div>
		    <script type="text/javascript">
				jQuery(document).ready(function($) {
					$( '#wooslider-id-666' ).flexslider({
						namespace: "wooslider-",
						animation: "fade",
						useCSS: true,
						slideshowSpeed: 7000,
						animationSpeed: 600,
						controlNav: true,
						slideshow: true,
						video: true,
						directionNav: true,
						keyboard: false,
						mousewheel: false,
						pausePlay: false,
						animationLoop: true,
						pauseOnAction: true,
						pauseOnHover: true,
						smoothHeight: true,
						touch: true,
						prevText: "Zurück",
						nextText: "Vor",
						playText: "Start",
						start: function(slider){
							var wooslider_holder = $(slider).find("li.slide");
							if(0 !== wooslider_holder.length) {
								var wooslides = ([]).concat(wooslider_holder.splice(0,2), wooslider_holder.splice(-2,2), $.makeArray(wooslider_holder));
								$.each(wooslides, function(i,el){
									var content = $(this).attr("data-wooslidercontent");
									if(typeof content == "undefined" || false == content) return;
									$(this).append(content).removeAttr("data-wooslidercontent");
								});
							}
							//$(slider).fitVids();
							var maxHeight = 0;
							$(slider).find(".wooslider-control-nav li").each(function(i,el) { maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height(); });
							$(slider).css("margin-bottom", maxHeight + 20 + "px");
						},
						before: function(slider){},
						after: function(slider){},
						end: function(slider){},
						added: function(slider){},
						removed: function(slider){}
					});
				});
			</script>
		    <?php wp_reset_postdata(); ?>
	<?php endif;
	//END: Slider
	?>

	<div id="content" class="container">

		<h1><?php post_type_archive_title(); ?></h1>
		<?php $archive_obj = get_post_type_object(get_post_type()); ?>
		<?php echo wpautop($archive_obj->description); ?>

	<?php if (have_posts()): ?>

		<?php if ( is_active_sidebar( 'product-filter' ) ) : ?>
			<div class="row">
				<div class="col-md-9">
		<?php endif; ?>

					<div class="row facetwp-template" id="product-tiles">
					<?php while (have_posts()): ?>
						<?php the_post(); ?>
						<?php $categories = get_the_category(); ?>
						<div class="col-md-4">
							<div class="thumbnail">
								<a href="<?php the_permalink(); ?>" <?php if ( ! empty( $categories ) ) : echo 'class="category-'.$categories[0]->slug.'"'; endif; ?>><?php the_post_thumbnail('medium'); ?></a>
								<div class="caption">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					</div>

					<?php echo facetwp_display( 'pager' ); ?>

					<?php // the_posts_pagination( array( 'mid_size' => 3, 'prev_text' => '', 'next_text' => '', 'screen_reader_text' => '' ) ); ?>

		<?php if ( is_active_sidebar( 'product-filter' ) ) : ?>
				</div>
				<div class="col-md-3">
					<div id="sidebar">
						<?php dynamic_sidebar( 'product-filter' ); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	</div>

<?php get_footer(); ?>
