/* Custom JS Functions */
jQuery(document).ready(function($) {

	//wooslider workaround for links within slides
	$( '.wooslider .slide-content li a, .wooslider .slide-content p a' ).click(function(event) {
		event.preventDefault();
		var linktarget = $(this).attr('href');
		window.location.href=linktarget;
	});
});