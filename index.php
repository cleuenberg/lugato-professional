<?php
/**
 * The main template file
 */

get_header(); ?>

	<div id="content" class="container">
		<?php
		if (have_posts()): while (have_posts()): the_post();
			the_content();
		endwhile; endif;
		?>
	</div>

<?php get_footer(); ?>