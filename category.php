<?php
/**
 * The standard template for displaying category pages
 */

get_header(); ?>

	<?php if (have_posts()): ?>

		<?php //wooslider(); ?>
			<div id="wooslider-id-666" class="wooslider wooslider-id-666 wooslider-type-attachments wooslider-theme-default">
		        <ul class="slides">
		        	<?php while (have_posts()): ?>
		        		<?php the_post(); ?>
		                <li class="slide slide-<?php echo $post->post_name; ?>">
		                	<div class="slide-image">
		                		<?php
		                		$slider_image = get_field('slider_image', $post->ID);
								$slider_image_size = 'full'; // (thumbnail, medium, large, full or custom size)
								if ($slider_image) {
		                			echo wp_get_attachment_image( $slider_image, $slider_image_size );
		                		} else {
		                			echo '<img src="/wp-content/themes/lugato/img/slider_hintergrund.jpg">';
		                		}
		                		?>
		                	</div>
		                	<div class="slide-content">
		                    	<h2><?php the_title(); ?></h2>
		                    	<?php if( have_rows('usps') ): ?>
		                    		<?php $i = 0; ?>
		                    		<ul>
								    <?php while( have_rows('usps') ) : the_row(); ?>
								    	<?php $i++; if ($i > 4) { break; } ?>
								    	<li><?php echo get_sub_field('usp'); ?></li>
								    <?php endwhile; ?>
									</ul>
								<?php endif; ?>
		                    	<p><a href="<?php the_permalink(); ?>">Mehr Infos zum Produkt</a></p>
		                    </div>
		                </li>
		            <?php endwhile; ?>
		        </ul>
		    </div>
		    <script type="text/javascript">
				jQuery(document).ready(function($) {
					$( '#wooslider-id-666' ).flexslider({
						namespace: "wooslider-",
						animation: "fade",
						useCSS: true,
						slideshowSpeed: 7000,
						animationSpeed: 600,
						controlNav: true,
						slideshow: true,
						video: true,
						directionNav: true,
						keyboard: false,
						mousewheel: false,
						pausePlay: false,
						animationLoop: true,
						pauseOnAction: true,
						pauseOnHover: true,
						smoothHeight: true,
						touch: true,
						prevText: "Zurück",
						nextText: "Vor",
						playText: "Start",
						start: function(slider){
							var wooslider_holder = $(slider).find("li.slide");
							if(0 !== wooslider_holder.length) {
								var wooslides = ([]).concat(wooslider_holder.splice(0,2), wooslider_holder.splice(-2,2), $.makeArray(wooslider_holder));
								$.each(wooslides, function(i,el){
									var content = $(this).attr("data-wooslidercontent");
									if(typeof content == "undefined" || false == content) return;
									$(this).append(content).removeAttr("data-wooslidercontent");
								});
							}
							//$(slider).fitVids();
							var maxHeight = 0;
							$(slider).find(".wooslider-control-nav li").each(function(i,el) { maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height(); });
							$(slider).css("margin-bottom", maxHeight + 20 + "px");
						},
						before: function(slider){},
						after: function(slider){},
						end: function(slider){},
						added: function(slider){},
						removed: function(slider){}
					});
				});
			</script>
		<?php wp_reset_postdata(); ?>

	<?php endif; ?>

	<div id="content" class="container">

		<h1><?php single_cat_title(); ?></h1>
		<?php echo category_description(); ?> 

	<?php if (have_posts()): ?>
		<div class="row" id="product-tiles">
		<?php while (have_posts()): ?>
			<?php the_post(); ?>
			<div class="col-md-4">
				<div class="thumbnail">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
					<div class="caption">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
		</div>
	<?php endif; ?>

	</div>

<?php get_footer(); ?>
