<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="container">

		<?php if ( has_nav_menu( 'main' ) ) : ?>
		<nav class="navbar navbar-default" id="navigation">
			<div class="network pull-right hidden-xs">
				<a href="https://www.lugato.de" target="_blank">LUGATO.de</a>
			</div>
			<div class="hotline pull-right">
				<a href="tel:<?php echo get_field('phone_number_link', 'option'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo get_field('phone_number', 'option'); ?></a> <small><?php echo get_field('phone_text', 'option'); ?></small>
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo get_bloginfo('url'); ?>">
					<img alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo get_field('logo', 'option'); ?>" class="img-responsive">
				</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-collapse-1">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'main',
						'menu_class'     => 'nav navbar-nav navbar-right',
					 ) );
				?>
			</div>

			<?php
				wp_nav_menu( array(
					'theme_location' => 'meta',
					'menu_class'     => 'nav nav-pills nav-justified',
				) );
			?>
		</nav>
		<?php endif; ?>
