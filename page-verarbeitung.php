<?php
/**
 * The standard template for displaying pages
 */

get_header(); ?>

	<?php if (have_posts()): ?>
		<?php while (have_posts()): ?>
			<div id="content" class="container">

				<?php if (!is_front_page()) : ?>
					<h1><?php the_title(); ?></h1>
				<?php endif; ?>

				<?php the_post(); ?>
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>

	<?php
	// args
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'produkte',
		'meta_query'	=> array(
			'relation'		=> 'OR',
			array(
				'key'		=> 'untergrundvorbereitung',
				'value'		=> array(''),
				'compare' 	=> 'NOT IN'
			),
			array(
				'key'		=> 'vorbereitung',
				'value'		=> array(''),
				'compare' 	=> 'NOT IN'
			),
			array(
				'key'		=> 'verarbeitung',
				'value'		=> array(''),
				'compare' 	=> 'NOT IN'
			)
		)
	);


	// query
	$the_query = new WP_Query( $args );
	?>

	<?php if( $the_query->have_posts() ): ?>

	<div class="container">
		<div class="row" id="product-tiles">

			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $categories = get_the_category(); ?>
				<div class="col-md-4">
					<div class="thumbnail">
						<a href="<?php the_permalink(); ?>" <?php if ( ! empty( $categories ) ) : echo 'class="category-'.$categories[0]->slug.'"'; endif; ?>><?php the_post_thumbnail('medium'); ?></a>
						<div class="caption">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					</div>
				</div>
			<?php endwhile; ?>

		</div>
	</div>

	<?php endif; ?>
	<?php wp_reset_query(); ?>

<?php get_footer(); ?>
