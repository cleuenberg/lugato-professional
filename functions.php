<?php
/**
 * Enqueues scripts and styles.
 *
 */
function lugato_scripts() {
	// Bootstrap stylesheet
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.4.1' );

	// FontAwesome stylesheet
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );

	// Theme stylesheet
	wp_enqueue_style( 'lugato-style', get_stylesheet_uri(), array(), '20240527' );

	// Bootstrap scripts
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.4.1', true );

	// Theme scripts
	wp_enqueue_script( 'lugato-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20170215', true );

	// Flexslider scripts
	if ( is_front_page() || is_category() || is_post_type_archive('produkte') ) {
		wp_enqueue_style( 'flexslider-style', get_template_directory_uri() . '/flexslider/flexslider.css', array(), '2.7.2' );
		wp_enqueue_style( 'wooslider-style', get_template_directory_uri() . '/flexslider/wooslider.style.css', array(), '2.5.0' );
		wp_enqueue_script( 'flexslider-script', get_template_directory_uri() . '/flexslider/jquery.flexslider-min.js', array( 'jquery' ), '2.7.2', true );
	}
}
add_action( 'wp_enqueue_scripts', 'lugato_scripts' );


// Registering the menus
register_nav_menus( array(
	'main' => __( 'Main Menu', 'lugato' ),
	'meta' => __( 'Product Menu', 'lugato' ),
	'footer' => __( 'Footer Menu', 'lugato' ),
) );


// Registering the widgets
function lugato_widgets_init() {
	register_sidebar( array(
		'name'          => 'Product Filter',
		'id'            => 'product-filter',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
}
add_action( 'widgets_init', 'lugato_widgets_init' );


// Security related
remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
add_filter('xmlrpc_enabled', '__return_false');
define('DISALLOW_FILE_EDIT', true);


// Shortcode: Grid System - Row
function lugato_shortcode_row( $atts, $content = null ) {

	// puzzle markup
	$html = '<div class="row">' . do_shortcode($content) . '</div>';

	// return the markup
	return $html;
}
add_shortcode( 'row', 'lugato_shortcode_row' );


// Shortcode: Grid System - Columns
function lugato_shortcode_column( $atts, $content = null ) {

	// attributes
	extract(shortcode_atts(
		array(
			'size' => 6
		), $atts )
	);

	// puzzle markup
	$html = '<div class="col-md-'.$size.'">' . wpautop(do_shortcode($content)) . '</div>';

	// return the markup
	return $html;
}
add_shortcode( 'column', 'lugato_shortcode_column' );


// Shortcode: Icons
function lugato_shortcode_icon( $atts ) {

	// attributes
	extract(shortcode_atts(
		array(
			'icon' => 'zahnrad'
		), $atts )
	);

	// puzzle markup
	$html = '<i class="lugato-icons icon-' . $icon . '"></i>';

	// return the markup
	return $html;
}
add_shortcode( 'icon', 'lugato_shortcode_icon' );


// Remove WP version number
function lugato_remove_version() {
	return '';
}
add_filter('the_generator', 'lugato_remove_version');


// Add ACF Theme Options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Register Google Api-Key for ACF Pro
function lugato_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyC_HX8byPQ970h-J1fEOeNFGRfMYE3QbFg');
}
add_action('acf/init', 'lugato_acf_init');