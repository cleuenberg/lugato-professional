<?php
/**
 * The standard template for displaying pages
 */

get_header(); ?>

	<?php if (is_front_page()) : ?>

		<?php
			//START: Slider
			$sliderObject = get_field('slider');

			if( $sliderObject ): ?>
				<?php //wooslider(); ?>
				<div id="wooslider-id-666" class="wooslider wooslider-id-666 wooslider-type-attachments wooslider-theme-default">
			        <ul class="slides">
			            <?php foreach( $sliderObject as $post ): ?>
			            	<?php
			            		//meta fields
			            		$slide_meta = get_post_meta( $post->ID );
			            	?>
			                <li class="slide slide-<?php echo $post->post_name; ?>">
			                	<div class="slide-image">
			                		<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
			                	</div>
			                	<div class="slide-content">
			                    	<h2><?php echo $post->post_title; ?></h2>
			                    	<?php echo $post->post_content; ?>
			                    	<p><a href="<?php echo get_field('slide_url'); ?>">Alle Produkte zum <?php echo $post->post_title; ?></a></p>
			                    </div>
			                </li>
			            <?php endforeach; ?>
			        </ul>
			    </div>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$( '#wooslider-id-666' ).flexslider({
							namespace: "wooslider-",
							animation: "fade",
							useCSS: true,
							slideshowSpeed: 7000,
							animationSpeed: 600,
							controlNav: true,
							slideshow: true,
							video: true,
							directionNav: true,
							keyboard: false,
							mousewheel: false,
							pausePlay: false,
							animationLoop: true,
							pauseOnAction: true,
							pauseOnHover: true,
							smoothHeight: true,
							touch: true,
							prevText: "Zurück",
							nextText: "Vor",
							playText: "Start",
							start: function(slider){
								var wooslider_holder = $(slider).find("li.slide");
								if(0 !== wooslider_holder.length) {
									var wooslides = ([]).concat(wooslider_holder.splice(0,2), wooslider_holder.splice(-2,2), $.makeArray(wooslider_holder));
									$.each(wooslides, function(i,el){
										var content = $(this).attr("data-wooslidercontent");
										if(typeof content == "undefined" || false == content) return;
										$(this).append(content).removeAttr("data-wooslidercontent");
									});
								}
								//$(slider).fitVids();
								var maxHeight = 0;
								$(slider).find(".wooslider-control-nav li").each(function(i,el) { maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height(); });
								$(slider).css("margin-bottom", maxHeight + 20 + "px");
							},
							before: function(slider){},
							after: function(slider){},
							end: function(slider){},
							added: function(slider){},
							removed: function(slider){}
						});
					});
				</script>
			    <?php wp_reset_postdata(); ?>
		<?php endif;
		//END: Slider
		?>

	<?php elseif (is_page(6)) : ?>

		<?php //START: Google Map ?>
		<?php if (get_field('google_map_shortcode')) : ?>
			<div class="acf-map">
				<?php echo do_shortcode( get_field('google_map_shortcode') ); ?>
			</div>
		<?php endif; ?>
		<?php //END: Google Map ?>

	<?php endif; ?>

	<?php if (have_posts()): ?>
		<?php while (have_posts()): ?>
			<div id="content" class="container">

				<?php if (!is_front_page()) : ?>
					<h1><?php the_title(); ?></h1>
				<?php endif; ?>

				<?php the_post(); ?>
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
