<?php
/**
 * The standard template for displaying produkte
 */

get_header(); ?>

</div>

	<?php if (have_posts()): ?>
		<?php while (have_posts()): ?>
			<?php $product_cats = get_the_category(); $product_cat_slug = $product_cats[0]->slug; ?>
			<div id="content" class="container-fluid cat-<?php echo $product_cat_slug; ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<p id="breadcrumbs">','</p>');
							} ?>
						</div>
						<div class="col-md-8">
							<?php the_post(); ?>
							<h1><?php the_title(); ?></h1>
							
							<?php 
							$rows = get_field('usps');
							if($rows) {
								echo '<ul class="list-usps">';

								foreach($rows as $row) {
									echo '<li>' . $row['usp'] . '</li>';
								}

								echo '</ul>';
							} ?>

							<?php if (get_field('anwendung')) : ?>
								<h2>Anwendung</h2>
								<?php echo get_field('anwendung'); ?>
							<?php endif; ?>

							<?php if (get_field('lieferform')) : ?>
								<h2>Lieferform</h2>
								<?php echo get_field('lieferform'); ?>
							<?php endif; ?>



							<?php if (get_field('untergrundvorbereitung')) : ?>
								<h2>Untergrundvorbereitung</h2>
								<?php echo get_field('untergrundvorbereitung'); ?>
							<?php endif; ?>

							<?php if (get_field('vorbereitung')) : ?>
								<h2>Vorbereitung</h2>
								<?php echo get_field('vorbereitung'); ?>
							<?php endif; ?>

							<?php if (get_field('verarbeitung')) : ?>
								<h2>Verarbeitung</h2>
								<?php echo get_field('verarbeitung'); ?>
							<?php endif; ?>

							<?php if (get_field('hinweis')) : ?>
								<p><strong><?php echo get_field('hinweis'); ?></strong></p>
							<?php endif; ?>
						</div>
						<div class="col-md-4">
							<div class="product-img center-block">
								<?php the_post_thumbnail('large', ['class' => 'img-responsive']); ?>
							</div>

							<?php 
							$rows = get_field('downloads');
							if($rows) {
								echo '<div class="text-center">';

								foreach($rows as $row) {
									$filesize = filesize( get_attached_file( $row['download']['ID'] ) );
									$filesize = size_format( $filesize );
									echo '<p><a class="btn btn-default btn-download" href="' . $row['download']['url'] . '"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> ' . $row['download_label'] . '<span>PDF, ' . $filesize . '</span></a></p>';
								}

								echo '</div>';
							} ?>
						</div>
					</div>
				</div>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>